<?php

/**
 * Поиск самого дорогого и самого дешевого товара, которые не были заказаны за прошлый месяц.
 * 
 * Доп фильтрация по ID при совпадении цены - это же тестовое задание ;), а не решение продакшн.
 * Добавьте еще сортировку/доп.фильтрацию.
 *
 * 
 * При выборке заказов поставлен 'limit' => 300, можно убрать, важно убедиться в том,
 * чтобы процесс завершился раньше, чем отвалится по таймауту. 
 * При большом объеме данных запускать лучше в режиме cli с необходимым логированием (в файл / почту и т.п.). 
 * 
 * Лучше выполнять не через админку битрикса, а как отдельный скрипт. 
 */

set_time_limit(0);

if (php_sapi_name() == 'cli') {
    $_SERVER['DOCUMENT_ROOT'] = '/home/bitrix/www';
}

require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
if (!\Bitrix\Main\Loader::includeModule('catalog')) {
    echo 'Error with iblock module!';
    die;
}
while (ob_get_level()) {
    ob_end_flush();
}

require_once $_SERVER['DOCUMENT_ROOT'].'/kint.php'; // для отладки

/**
 * получение заказов за прошлый месяц.
 * NB: выборка за прошлый месяц в комментарии (в примере взят январь 2021 ).
 */
$db_sales = \Bitrix\Sale\Order::getList([
    'select' => ['ID'],
    'filter' => [
        '>=DATE_INSERT' => '01.01.2021', // date("d.m.Y", strtotime("first day of previous month"));
        '<=DATE_INSERT' => '01.31.2021', // date("d.m.Y", strtotime("last day of previous month"));
    ],
    'limit' => 300, // NB: лимит поставлен для ускорения выборки. 
])->fetchAll();
$order_ids = array_column($db_sales, 'ID');
// d($order_ids);

/*
 * получение id товаров по отобранным заказм
 */
foreach ($order_ids as $order_id) {
    $basketRes = \Bitrix\Sale\Internals\BasketTable::getList([
        'filter' => [
            'ORDER_ID' => $order_id,
        ],
    ]);

    while ($item = $basketRes->fetch()) {
        $ids[] = $item['PRODUCT_ID'];
    }
}
$ids = array_unique($ids);
sort($ids);

/**
 * Поскольку типов цен может быть несколько, необходимо получить их id.
 * Для примера выбирается первое значение id типа.
 */
$rsGroup = \Bitrix\Catalog\GroupTable::getList();
if ($arGroup = $rsGroup->fetch()) {
    $price_id = $arGroup['ID'];
} else {
    die('что-то не так с типами цен.');
}

/**
 * Находим самый дорогой товар, исключая массив id товаров, которые были заказаны за прошлый месяц.
 * В примере дополнительно отфильтрована цена (больше 0) и статус активности элемента. 
 */
$db_res = CIBlockElement::GetList(
    ['CATALOG_PRICE_'.$price_id => 'DESC'],
    ['IBLOCK_ID' => IBLOCK_CATALOG, '!ID' => $ids, '=ACTIVE' => 'Y', '>CATALOG_PRICE_'.$price_id => 0],
    false, false,
    ['ID', 'NAME', 'ACTIVE', 'CATALOG_PRICE_'.$price_id]
);
if ($ar_res = $db_res->Fetch()) {
    echo $ar_res['ID'],' | ',$ar_res['NAME'],' | ',$ar_res['CATALOG_PRICE_'.$price_id],'<br>';
}
/**
 * Находим самый дешевый товар, исключая массив id товаров, которые были заказаны за прошлый месяц.
 * В примере дополнительно отфильтрована цена (больше 0) и статус активности элемента. 
 */
$db_res = CIBlockElement::GetList(
    ['CATALOG_PRICE_'.$price_id => 'ASC'],
    ['IBLOCK_ID' => IBLOCK_CATALOG, '!ID' => $ids, '=ACTIVE' => 'Y', '>CATALOG_PRICE_'.$price_id => 0],
    false, false,
    ['ID', 'NAME', 'ACTIVE', 'CATALOG_PRICE_'.$price_id]
);
if ($ar_res = $db_res->Fetch()) {
    echo $ar_res['ID'],' | ',$ar_res['NAME'],' | ',$ar_res['CATALOG_PRICE_'.$price_id],'<br>';
}
die;
